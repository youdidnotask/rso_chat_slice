#ifndef __ChatI_h__
#define __ChatI_h__

#include <Chat.h>

namespace Chat
{

class ChatServerI : virtual public ChatServer
{
public:

    virtual void LogIn(const ::Chat::UserPrx&,
                       const Ice::Current&);

    virtual ::Chat::UserPrx GetUserByName(const ::std::string&,
                                          const Ice::Current&);

	virtual void RedirectPrivateText(const ::std::string&,
								  	 const ::std::string&,
								 	 const ::Chat::UserPrx&,
                                     const Ice::Current&);

    virtual ::Chat::Groups GroupList(const Ice::Current&);

    virtual ::Chat::GroupServerPrx GetGroupServerByName(const ::std::string&,
                                                        const Ice::Current&);

    virtual void CreateGroup(const ::Chat::GroupServerPrx&,
                             const Ice::Current&);

    virtual void DeleteGroup(const ::std::string&,
                             const Ice::Current&);

    virtual void RegisterServer(const ::Chat::GroupServerManagerPrx&,
                                const Ice::Current&);

    virtual void UnregisterServer(const ::Chat::GroupServerManagerPrx&,
                                  const Ice::Current&);

private:
	::Chat::Users _users;
	::Chat::GroupManagers _groupManagers;

	virtual bool VerifyUserName(const ::std::string&,
								const Ice::Current&);

	virtual bool VerifyGroupServerManager(const ::Chat::GroupServerManagerPrx&,
                            			  const Ice::Current&);

};

class GroupServerI : virtual public GroupServer
{
public:
	GroupServerI(const ::std::string&);

    virtual void Join(const ::Chat::UserPrx&,
                      const Ice::Current&);

    virtual void Leave(const ::Chat::UserPrx&,
                       const Ice::Current&);

    virtual void SendMessage(const ::std::string&,
                             const ::Chat::UserPrx&,
                             const Ice::Current&);

    virtual ::Chat::Users UserList(const Ice::Current&);

    virtual ::std::string Name(const Ice::Current&);

private:
	::std::string _name;
	::Chat::Users _users;

	virtual bool VerifyUser(const ::Chat::UserPrx&,
            			    const Ice::Current&);

};

class GroupServerManagerI : virtual public GroupServerManager
{
public:
	GroupServerManagerI(const ::Ice::Long);
	
    virtual void CreateGroup(const ::Chat::GroupServerPrx&,
                                               const Ice::Current&);

    virtual ::Chat::Groups ListGroups(const Ice::Current&);

    virtual void DeleteGroup(const ::std::string&,
                             const Ice::Current&);

    virtual ::Chat::GroupServerPrx GetGroupServerByName(const ::std::string&,
                                                        const Ice::Current&);

private:
	::Ice::Long	_number;
	::Chat::Groups _groups;

	virtual bool VerifyGroupName(const ::std::string&,
								 const Ice::Current&);

};

class UserI : virtual public User
{
public:
	UserI(const ::std::string&);

    virtual ::std::string GetName(const Ice::Current&);

    virtual void ReceiveText(const ::std::string&,
                             const ::Chat::UserPrx&,
                             const ::std::string&,
                             const Ice::Current&);

    virtual void ReceivePrivateText(const ::std::string&,
                                    const ::Chat::UserPrx&,
                                    const Ice::Current&);

private:
	::std::string _username;
};

}

#endif
