module Chat {

	interface GroupServerManager;
	interface GroupServer;
	interface User;

	exception NameDoesNotExist{};
	exception NameAlreadyExists{};
	exception ServerAlreadyRegistered{};
	exception ServerDoesNotExist{};
	exception UserAlreadyRegistered{};
	exception UserDoesNotExist{};

	sequence<GroupServerManager*> GroupManagers;
	sequence<GroupServer*> Groups;
	sequence<User*> Users; 

	interface ChatServer {
		void LogIn(User* callback) throws NameAlreadyExists;
		User* GetUserByName(string name);
		void RedirectPrivateText(string message, string username, User* sender) throws UserDoesNotExist;
		Groups GroupList();
		GroupServer* GetGroupServerByName(string name) throws NameDoesNotExist;
		void CreateGroup(GroupServer* group) throws NameAlreadyExists;
		void DeleteGroup(string name) throws NameDoesNotExist;
		void RegisterServer(GroupServerManager* serverManager) throws ServerAlreadyRegistered;
		void UnregisterServer(GroupServerManager* serverManager) throws ServerDoesNotExist;
	};

	interface GroupServer {
		void Join(User* who)  throws UserAlreadyRegistered;
		void Leave(User* who) throws UserDoesNotExist;
		void SendMessage(string message, User* sender) throws UserDoesNotExist;
		Users UserList();
		string Name();
	};

	interface GroupServerManager {
		void CreateGroup(GroupServer* group) throws NameAlreadyExists;
		Groups ListGroups();
		void DeleteGroup(string name) throws NameDoesNotExist;
		GroupServer* GetGroupServerByName(string name) ;
	};

	interface User {
		string GetName();
		void ReceiveText(string msg, User* sender, string group);
		void ReceivePrivateText(string msg, User* sender);
	};

};
