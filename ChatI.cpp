
#include <ChatI.h>
#include <vector>

bool 
Chat::ChatServerI::VerifyUserName(const ::std::string& name,
			   					  const Ice::Current&) {
	int i;
    for(i=0; i<_users.size(); ++i) {
		if(_users[i]->GetName() == name) {
			return false;
		}
	}
	return true;
};

bool 
Chat::ChatServerI::VerifyGroupServerManager(const ::Chat::GroupServerManagerPrx& serverManager,
                            			    const Ice::Current& current) {
	int i;
    for(i=0; i<_groupManagers.size(); ++i) {
		if(_groupManagers[i] == serverManager) {
			return false;
		}
	}
	return true;	
}

void
Chat::ChatServerI::LogIn(const ::Chat::UserPrx& callback,
                         const Ice::Current& current)
{
	if(VerifyUserName(callback->GetName(), current)) {
		_users.push_back(callback);
		callback->ReceivePrivateText("Welcome " + callback->GetName() + "!", NULL);
	}
	else {
		::Chat::NameAlreadyExists ex;
		throw ex;
	}
}

::Chat::UserPrx
Chat::ChatServerI::GetUserByName(const ::std::string& name,
                                 const Ice::Current& current)
{
	int i;
    for(i=0; i<_users.size(); ++i) {
		if(_users[i]->GetName() == name) {
			return _users[i];
		}
	}
	return NULL;
}

void 
Chat::ChatServerI::RedirectPrivateText(const ::std::string& message,
									   const ::std::string& username,
									   const ::Chat::UserPrx& sender,
                                       const Ice::Current& current)
{
	if(!VerifyUserName(username, current)) {
		GetUserByName(username, current)->ReceivePrivateText(message, sender);
	}
	else {
		::Chat::UserDoesNotExist ex;
		throw ex;
	}
}


::Chat::Groups
Chat::ChatServerI::GroupList(const Ice::Current& current)
{
	::Chat::Groups allGroups;
	int i, j;
    for(i=0; i<_groupManagers.size(); ++i) {
//::std::cout << "I am still live" << ::std::endl;
//allGroups = _groupManagers[i]->ListGroups();
//allGroups.assign(_groupManagers[i]->ListGroups().begin(), _groupManagers[i]->ListGroups().end());

		for(j=0; j<_groupManagers[i]->ListGroups().size(); ++j) {
			allGroups.push_back(_groupManagers[i]->ListGroups()[j]);
		}
//::std::cout << "SIZE: " + allGroups.size() << ::std::endl;
	}	
    return allGroups;
}

::Chat::GroupServerPrx
Chat::ChatServerI::GetGroupServerByName(const ::std::string& name,
                                        const Ice::Current& current)
{
	::Chat::Groups allGroups = GroupList(current);
    int i;
	for(i=0; i<allGroups.size(); ++i) {
		if(allGroups[i]->Name() == name) {
			return allGroups[i];
		}
	}
	::Chat::NameDoesNotExist ex;
	throw ex;
}

void
Chat::ChatServerI::CreateGroup(const ::Chat::GroupServerPrx& group,
                               const Ice::Current& current)
{
	_groupManagers[0]->CreateGroup(group);
}

void
Chat::ChatServerI::DeleteGroup(const ::std::string& name,
                               const Ice::Current& current)
{
//::std::cout <<"first step" << ::std::endl;
	_groupManagers[0]->DeleteGroup(name);
//::std::cout <<"last step" << ::std::endl;
}

void
Chat::ChatServerI::RegisterServer(const ::Chat::GroupServerManagerPrx& serverManager,
                                  const Ice::Current& current)
{
	if(VerifyGroupServerManager(serverManager, current)) {
		_groupManagers.push_back(serverManager);
	}
	else {
		::Chat::ServerAlreadyRegistered ex;
		throw ex;
	}
}

void
Chat::ChatServerI::UnregisterServer(const ::Chat::GroupServerManagerPrx& serverManager,
                                    const Ice::Current& current)
{
	if(!VerifyGroupServerManager(serverManager, current)) {
		//int i;
		//for(i=0; i<_groupManagers.size(); ++i) {
		//	if(_groupManagers[i] == serverManager) {
		//		_groupManagers.erase(_groupManagers.begin() + i-1);
		//	}
		//}
		for(::std::vector<Chat::GroupServerManagerPrx>::iterator i = _groupManagers.begin(); i!=_groupManagers.end(); i++) {
			if(*i == serverManager) {
				_groupManagers.erase(i);
			}
		}
	}
	else {
		::Chat::ServerDoesNotExist ex;
		throw ex;
	}
}

Chat::GroupServerI::GroupServerI(const std::string& name)
{
	_name = name;
}

bool 
Chat::GroupServerI::VerifyUser(const ::Chat::UserPrx& user,
            			       const Ice::Current& current) 
{
	int i;
	for(i=0; i<_users.size(); ++i) {
		if(_users[i]==user) {
			return false;
		}
	}
	return true;
}

void
Chat::GroupServerI::Join(const ::Chat::UserPrx& who,
                         const Ice::Current& current)
{
	if(VerifyUser(who, current)) {
		_users.push_back(who);
	}
	else {
		::Chat::UserAlreadyRegistered ex;
		throw ex;
	}
}

void
Chat::GroupServerI::Leave(const ::Chat::UserPrx& who,
                          const Ice::Current& current)
{
	if(!VerifyUser(who, current)) {
		int i;
		for(i=0; i<_users.size(); ++i) {
			if(_users[i] == who) {
				_users.erase(_users.begin()+i);
				return;
			}
		}
	}
	else {
		::Chat::UserDoesNotExist ex;
		throw ex;
	}
}

void
Chat::GroupServerI::SendMessage(const ::std::string& message,
                                const ::Chat::UserPrx& sender,
                                const Ice::Current& current)
{
	int i;
	for(i=0; i<_users.size(); ++i) {
		_users[i]->ReceiveText(message, sender, this->Name(current));
	}
}

::Chat::Users
Chat::GroupServerI::UserList(const Ice::Current& current)
{
    return _users;
}

::std::string
Chat::GroupServerI::Name(const Ice::Current& current)
{
    return _name;
}

Chat::GroupServerManagerI::GroupServerManagerI(::Ice::Long num) {
	_number = num;
}

bool 
Chat::GroupServerManagerI::VerifyGroupName(const ::std::string& name,
			   					  		   const Ice::Current&) {
//::std::cout <<"VERIFY first step" << ::std::endl;
	int i;
    for(i=0; i<_groups.size(); ++i) {
		if(_groups[i]->Name() == name) {
			return false;
		}
	}
	return true;
//::std::cout <<"VERIFY last step" << ::std::endl;
};

void
Chat::GroupServerManagerI::CreateGroup(const ::Chat::GroupServerPrx& group,
                                       const Ice::Current& current)
{
	if(VerifyGroupName(group->Name(), current)) {
		_groups.push_back(group);
	}
	else {
		::Chat::NameAlreadyExists ex;
		throw ex;
	}
}

::Chat::Groups
Chat::GroupServerManagerI::ListGroups(const Ice::Current& current)
{
    return _groups;
}

void
Chat::GroupServerManagerI::DeleteGroup(const ::std::string& name,
                                       const Ice::Current& current)
{
//::std::cout <<"DELETE first step" << ::std::endl;
	if(!VerifyGroupName(name, current)) {
		::Chat::GroupServerPrx groupPrx = GetGroupServerByName(name, current);
		//for(::std::vector<Chat::GroupServerPrx>::iterator i = _groups.begin(); i!=_groups.end(); i++) {
		//	if(*i == groupPrx) {
		//		_groups.erase(i);
		//	}
		//}
		int i;
		for(i=0; i<_groups.size(); ++i) {
			if(_groups[i] == groupPrx) {
				_groups.erase(_groups.begin()+i);
				return;
			}
		}

	}
	else {
		::Chat::NameDoesNotExist ex;
		throw ex;
	}
//::std::cout <<"DELETE last step" << ::std::endl;
}

::Chat::GroupServerPrx
Chat::GroupServerManagerI::GetGroupServerByName(const ::std::string& name,
                                                const Ice::Current& current)
{
//::std::cout <<"GET BY NAME first step" << ::std::endl;
	int i;
    for(i=0; i<_groups.size(); ++i) {
		if(!_groups[i]->Name().compare(name)) {
			::std::cout << _groups[i]->Name();
//::std::cout <<"GET BY NAME last step" << ::std::endl;
			return _groups[i];
		}
	}
	return NULL;
}

Chat::UserI::UserI(const std::string& username)
{
	_username = username;
}

::std::string
Chat::UserI::GetName(const Ice::Current& current)
{
    return _username;
}

void
Chat::UserI::ReceiveText(const ::std::string& msg,
                         const ::Chat::UserPrx& sender,
                         const ::std::string& group,
                         const Ice::Current& current)
{
	::std::string _sender = (sender) ? sender->GetName() : "CHAT SERVER";
	::std::cout <<  "GROUP " + group + "|" + _sender + ": " + msg << ::std::endl; 
}

void
Chat::UserI::ReceivePrivateText(const ::std::string& msg,
                                const ::Chat::UserPrx& sender,
                                const Ice::Current& current)
{
	::std::string _sender = (sender) ? sender->GetName() : "CHAT SERVER";
	::std::cout <<  _sender + ": " + msg << ::std::endl; 
}
