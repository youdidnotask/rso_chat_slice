#include <Ice/Ice.h>
#include <IceUtil/UUID.h>
#include <Chat.h>
#include <ChatI.h>
#include <string>

using namespace std;
using namespace Chat;

class ChatApplication : virtual public Ice::Application {

private:
	Ice::Identity identity;
	::Chat::UserPrx userPrx;
	Ice::ObjectAdapterPtr adapter;
	ChatServerPrx chatServer;
	
	bool login() {
		string username;
		cout << "Enter username:\t";
		cin >> username;
		::Chat::UserPtr user = new UserI(username);

		identity.name = IceUtil::generateUUID();
		adapter->add(user, identity);
		adapter->activate();

		userPrx = UserPrx::uncheckedCast(adapter->createProxy(identity));

		try {
			chatServer->LogIn(userPrx);
			return true;
		} catch(NameAlreadyExists ex) {
			cout << "Entered username is taken, please try again." << endl;
			return false;
		}
		//cout<< user->GetName();
	}

	bool menu() {
		int choice;
		displayGroups();
		cin.clear();
		cout << "[1]\tselect group" << endl;
		cout << "[2]\trefresh" << endl;
		cout << "[3]\tend" << endl;
		
		cin >> choice;
		switch(choice) {
			case 1:	
				selectGroup();
				return true;
			case 2:
				return true;
			case 3:
				return false;
			default:
				return true;
		}
	}
	
	void displayGroups() {
		::Chat::Groups g = chatServer->GroupList();
		int i;
		cout << "---------------------" << endl;
		cout << "GROUPS:" << endl;
		for(i=0; i<g.size(); ++i) {
			cout << "\t" + g[i]->Name() << endl;
		}
		cout << "---------------------" << endl;
	}

	void displayUsers(::Chat::GroupServerPrx gPrx) {
		::Chat::Users u = gPrx->UserList();

		int i;
		cout << "---------------------" << endl;
		cout << "USERS:" << endl;
		for(i=0; i<u.size(); ++i) {
			cout << "\t" + u[i]->GetName() << endl;
		}
		cout << "---------------------" << endl;
	}

	bool groupMenu(::Chat::GroupServerPrx gPrx) {
		int choice;
		displayUsers(gPrx);
		cin.clear();
		fflush(stdin);
		cout << "[1]\tsend message" << endl;
		cout << "[2]\tsend private message" << endl;
		cout << "[3]\tback" << endl;
		
		cin >> choice;
		switch(choice) {
			case 1:	
				sendMessage(gPrx);
				return true;
			case 2:
				sendPrivateMessage();
				return true;
			case 3:
				leaveGroup(gPrx);
				return false;
			default:
				return true;
		}
	}

	void selectGroup() {
		string groupName;
		cin.clear();
		cout << "Enter group name:\t";
		cin >> groupName;

		try {
			::Chat::GroupServerPrx gPrx = chatServer->GetGroupServerByName(groupName);
			gPrx->Join(userPrx);
			while(groupMenu(gPrx));
		} catch(NameDoesNotExist ex) {
			cout << "Invalid group name" << endl;
		} catch(UserAlreadyRegistered ex) {
			cout << "You are already in this group!" << endl;
		}	
	}

	void leaveGroup(::Chat::GroupServerPrx gPrx) {
		try {
			gPrx->Leave(userPrx);
		} catch(UserDoesNotExist ex) {
		}
	}

	void sendMessage(::Chat::GroupServerPrx gPrx) {
		string msg;
		cin.clear();
		cout << "Enter message:\t";
		cin >> msg;
		
		try {
			gPrx->SendMessage(msg, userPrx);
		} catch(UserDoesNotExist ex) {

		}
	}

	void sendPrivateMessage() {
		string user, msg;
		cin.clear();
		cout << "Enter user:\t";
		cin >> user;

		cin.clear();
		cout << "Enter message:\t";
		cin >> msg;

		try {
			chatServer->RedirectPrivateText(msg, user, userPrx);
		} catch(UserDoesNotExist ex) {
			cout << "Invalid user" << endl;
		}

	}

public:
    virtual int run(int, char*[]) {

		Ice::ObjectPrx base = communicator()->propertyToProxy("ChatServer.Proxy");
		chatServer = ChatServerPrx::checkedCast(base);
		if (!chatServer) {
			throw "Cannot connect with server";
		}		
		adapter = communicator()->createObjectAdapterWithEndpoints("ClientAdapter", "default -h localhost");
		chatServer->ice_getConnection()->setAdapter(adapter);
		chatServer->ice_getCachedConnection()->setACM(IceUtil::None, IceUtil::None, Ice::HeartbeatAlways);
		while(!login());
		while(menu());

		//Ice::ObjectAdapterPtr adapter = communicator()->createObjectAdapterWithEndpoints("ClientAdapter", "default -p 10002");
		//Ice::Identity ident;
		//ident.name = IceUtil::generateUUID();
		//::Chat::UserPtr user = new UserI();
		//adapter->add(user, ident);
		//adapter->activate();

		//::Chat::UserPrx userPrx = UserPrx::uncheckedCast(adapter->createProxy(ident));

		//chatServer->LogIn(userPrx);


        return 0;
    }



};

int
main(int argc, char* argv[])
{
    ChatApplication app;
    return app.main(argc, argv, "client.config");
}
