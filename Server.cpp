#include <Ice/Ice.h>
#include <Chat.h>
#include <ChatI.h>
#include <string>

using namespace std;
using namespace Chat;

class ChatApplication : virtual public Ice::Application {
private:
	Ice::ObjectAdapterPtr adapter;
	::Chat::ChatServerPrx chatServerPrx;
	::Chat::GroupServerManagerPrx groupServerManagerPrx;

	bool menu() {
		int choice;
		displayGroups();
		cout << "[1]\tadd" << endl;
		cout << "[2]\tdelete" << endl;
		cout << "[3]\tend" << endl;

		cin.clear();
		cin >> choice;
		switch(choice) {
			case 1:	
				addGroup();
				return true;
			case 2:
				deleteGroup();
				return true;
			case 3:
				return false;
			default:
				break;
		}
	}
	
	void displayGroups() {
		::Chat::Groups g = chatServerPrx->GroupList();
		int i;
		cout << "---------------------" << endl;
		cout << "GROUPS:" << endl;
		for(i=0; i<g.size(); ++i) {
			cout << "\t" + g[i]->Name() << endl;
		}
		cout << "---------------------" << endl;
	}

	void addGroup() {
		string groupName;
		cout << "Enter group name:\t";
		cin >> groupName;
	
		try {
			Ice::ObjectPtr object_group = new GroupServerI(groupName);
			::Chat::GroupServerPrx groupPrx = GroupServerPrx::uncheckedCast(adapter->addWithUUID(object_group));
			chatServerPrx->CreateGroup(groupPrx);
		}
		catch(NameAlreadyExists ex) {
			cout << "Entered name is taken" << endl;
		}
	}

	void deleteGroup() {
		string groupName;
		cout << "Enter group name:\t";
		cin >> groupName;		

		try {
			chatServerPrx->DeleteGroup(groupName);
		}
		catch(NameDoesNotExist ex) {
			cout << "Invalid name" << endl;
		}
	}

public:
    virtual int run(int, char*[]) {
		
		adapter = communicator()->createObjectAdapterWithEndpoints("ChatAdapter", "default -p 10000");

		Ice::ObjectPtr object_ChatServer = new ChatServerI();
		Ice::Identity identity_ChatServer;
		identity_ChatServer.name = "ChatServer";
		adapter->add(object_ChatServer, identity_ChatServer);

		Ice::ObjectPtr object_GroupServerManager = new GroupServerManagerI(1);
		Ice::Identity identity_GroupServerManager;
		identity_GroupServerManager.name = "GroupServerManager_01";		
		adapter->add(object_GroupServerManager, identity_GroupServerManager);

		adapter->activate();

		chatServerPrx = ChatServerPrx::uncheckedCast(adapter->createProxy(identity_ChatServer));
		groupServerManagerPrx = GroupServerManagerPrx::uncheckedCast(adapter->createProxy(identity_GroupServerManager));
		chatServerPrx->RegisterServer(groupServerManagerPrx);

		while(menu()) {}
/*
		Ice::ObjectPtr object_group = new GroupServerI("Group 1");
		::Chat::GroupServerPrx groupPrx = GroupServerPrx::uncheckedCast(adapter->addWithUUID(object_group));
		groupServerManagerPrx->CreateGroup(groupPrx);

		Ice::ObjectPtr object_group2 = new GroupServerI("Group 2");
		::Chat::GroupServerPrx groupPrx2 = GroupServerPrx::uncheckedCast(adapter->addWithUUID(object_group2));
		groupServerManagerPrx->CreateGroup(groupPrx2);

		::Chat::Groups g = groupServerManagerPrx->ListGroups();
		int i;
		for(i=0; i<g.size(); ++i) {
			cout << g[i]->Name() << endl;
		}		
		groupServerManagerPrx->DeleteGroup(groupPrx);
		g = groupServerManagerPrx->ListGroups();

		for(i=0; i<g.size(); ++i) {
			cout << g[i]->Name() << endl;
		}	
*/		

		communicator()->waitForShutdown();

        return 0;
    }
};

int
main(int argc, char* argv[])
{
    ChatApplication app;
    return app.main(argc, argv);
}
